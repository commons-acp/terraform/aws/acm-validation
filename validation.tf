
resource "aws_acm_certificate_validation" "cert_api" {
  provider                = aws-acm
  certificate_arn         = var.cert_arn
  validation_record_fqdns = [var.route53_fqdn]
}
resource "aws_api_gateway_domain_name" "api_gw_domain" {
  provider = aws-main
  certificate_arn = aws_acm_certificate_validation.cert_api.certificate_arn
  domain_name     = var.domain
}
