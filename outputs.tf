output "api_gateway_domain_name" {
  value = aws_api_gateway_domain_name.api_gw_domain.cloudfront_domain_name
}
output "api_gateway_zone_id" {
  value = aws_api_gateway_domain_name.api_gw_domain.cloudfront_zone_id
}