variable "route53_fqdn" {
  description = "record cname fqdn"
}
variable "cert_arn" {
  description = "certificate arn"
}
variable "domain" {
  description = ""
}